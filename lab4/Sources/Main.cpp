#define WIN32_LEAN_AND_MEAN
#include <windows.h> // for XMVerifyCPUSupport

#include "D3DApp.h"
#include "Application.h"
#include "d3dUtils.h"
#include <string>
#include <locale>

#ifdef _MSC_VER
#include <crtdbg.h>
bool CheckMemoryLeaks()
{
    return _CrtDumpMemoryLeaks();
}
#else
bool checkMemoryLeaks()
{
    return false;
}
#endif

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, INT showCmd)
{
    // Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    int status = 0;

    try
    {
        Application TheApp(hInstance);
        if (TheApp.Initialize())
        {
            status = TheApp.Run();
        }
    }
    catch (DxException& e)
    {
        MessageBox(nullptr, e.ToString().c_str(), L"HR Failed", MB_OK);
    }

#if defined(DEBUG) | defined(_DEBUG)
    if (CheckMemoryLeaks())
    {
        MessageBox(nullptr, L"Warning! Memory leaks detected.", L"HR Failed", MB_OK);
    }
#endif

    return status;
}
