#include "GeometryGenerator.h"
#include <DirectXMath.h>

using uint16 = GeometryGenerator::uint16;
using uint32 = GeometryGenerator::uint32;
using MeshData = GeometryGenerator::MeshData;
using Vertex = GeometryGenerator::Vertex;
using namespace DirectX;

std::vector<uint16>& GeometryGenerator::MeshData::GetIndices16()
{
	if (m_indices16.empty())
	{
		m_indices16.resize(Indices32.size());
		for (size_t i = 0; i < Indices32.size(); ++i)
			m_indices16[i] = static_cast<uint16>(Indices32[i]);
	}

	return m_indices16;
}

MeshData GeometryGenerator::CreateCylinder(const float bottomRadius, const float topRadius, const float height, const uint32 sliceCount, const uint32 stackCount)
{
	MeshData meshData;

	//
	// Build Stacks.
	// 

	const float stackHeight = height / static_cast<float>(stackCount);

	// Amount to increment radius as we move up each stack level from bottom to top.
	const float radiusStep = (topRadius - bottomRadius) / static_cast<float>(stackCount);

	const uint32 ringCount = stackCount + 1;

	// Compute vertices for each stack ring starting at the bottom and moving up.
	for (uint32 i = 0; i < ringCount; ++i)
	{
		const float y = i * stackHeight;
		const float r = bottomRadius + i * radiusStep;

		// vertices of ring
		const float dTheta = 2.0f * XM_PI / static_cast<float>(sliceCount);
		for (uint32 j = 0; j <= sliceCount; ++j)
		{
			Vertex vertex;

			const float c = cosf(j * dTheta);
			const float s = sinf(j * dTheta);

			vertex.Position = XMFLOAT3(r * c, y, r * s);

			// This is unit length.
			auto tangentU = XMFLOAT3(-s, 0.0f, c);

			const float dr = bottomRadius - topRadius;
			XMFLOAT3 bitangent(dr * c, -height, dr * s);

			const XMVECTOR T = XMLoadFloat3(&tangentU);
			const XMVECTOR B = XMLoadFloat3(&bitangent);
			const XMVECTOR N = XMVector3Normalize(XMVector3Cross(T, B));
			XMStoreFloat3(&vertex.Normal, N);
			vertex.Weight = i / static_cast<float>(stackCount);

			meshData.m_vertices.push_back(vertex);
		}
	}

	// Add one because we duplicate the first and last vertex per ring
	// since the texture coordinates are different.
	const uint32 ringVertexCount = sliceCount + 1;

	// Compute indices for each stack.
	for (uint32 i = 0; i < stackCount; ++i)
	{
		for (uint32 j = 0; j < sliceCount; ++j)
		{
			meshData.Indices32.push_back(i * ringVertexCount + j);
			meshData.Indices32.push_back((i + 1) * ringVertexCount + j);
			meshData.Indices32.push_back((i + 1) * ringVertexCount + j + 1);

			meshData.Indices32.push_back(i * ringVertexCount + j);
			meshData.Indices32.push_back((i + 1) * ringVertexCount + j + 1);
			meshData.Indices32.push_back(i * ringVertexCount + j + 1);
		}
	}

	BuildCylinderTopCap(topRadius, height, sliceCount, meshData);
	BuildCylinderBottomCap(bottomRadius, height, sliceCount, meshData);

	return meshData;
}

void GeometryGenerator::BuildCylinderTopCap(const float topRadius, const float height, const uint32 sliceCount,
                                            MeshData& meshData)
{
	const uint32 baseIndex = static_cast<uint32>(meshData.m_vertices.size());

	const float y = height;
	const float dTheta = 2.0f * XM_PI / static_cast<float>(sliceCount);

	// Duplicate cap ring vertices because the texture coordinates and normals differ.
	for (uint32 i = 0; i <= sliceCount; ++i)
	{
		const float x = topRadius * cosf(i * dTheta);
		const float z = topRadius * sinf(i * dTheta);

		meshData.m_vertices.push_back(Vertex(x, y, z, 0.0f, 1.0f, 0.0f, 1.0f));
	}

	// Cap center vertex.
	meshData.m_vertices.push_back(Vertex(0.0f, y, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f));

	// Index of center vertex.
	const uint32 centerIndex = static_cast<uint32>(meshData.m_vertices.size()) - 1;

	for (uint32 i = 0; i < sliceCount; ++i)
	{
		meshData.Indices32.push_back(centerIndex);
		meshData.Indices32.push_back(baseIndex + i + 1);
		meshData.Indices32.push_back(baseIndex + i);
	}
}

void GeometryGenerator::BuildCylinderBottomCap(const float bottomRadius, const float height, const uint32 sliceCount,
                                               MeshData& meshData)
{
	// 
	// Build bottom cap.
	//

	const uint32 baseIndex = static_cast<uint32>(meshData.m_vertices.size());
	constexpr float y = 0.0f;

	// vertices of ring
	const float dTheta = 2.0f * XM_PI / static_cast<float>(sliceCount);
	for (uint32 i = 0; i <= sliceCount; ++i)
	{
		const float x = bottomRadius * cosf(i * dTheta);
		const float z = bottomRadius * sinf(i * dTheta);

		meshData.m_vertices.push_back(Vertex(x, y, z, 0.0f, -1.0f, 0.0f, 0.0f));
	}

	// Cap center vertex.
	meshData.m_vertices.push_back(Vertex(0.0f, y, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f));

	// Cache the index of center vertex.
	const uint32 centerIndex = static_cast<uint32>(meshData.m_vertices.size()) - 1;

	for (uint32 i = 0; i < sliceCount; ++i)
	{
		meshData.Indices32.push_back(centerIndex);
		meshData.Indices32.push_back(baseIndex + i);
		meshData.Indices32.push_back(baseIndex + i + 1);
	}
}
