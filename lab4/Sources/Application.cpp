#include "Application.h"
#include <DirectXColors.h>
#include <d3dcompiler.h>
#include <fstream>

using namespace DirectX;

Application::Application(HINSTANCE hInstance) : Super(hInstance)
{
}

Application::~Application()
{
	if (m_device != nullptr)
		FlushCommandQueue();
}

bool Application::Initialize()
{
	if (!Super::Initialize())
		return false;

	ThrowIfFailed(m_commandList->Reset(m_directCmdListAlloc.Get(), nullptr));

	BuildRootSignature();
	BuildShadersAndInputLayout();
	BuildShapeGeometry();
	BuildRenderItems();
	BuildFrameResources();
	BuildDescriptorHeaps();
	BuildConstantBufferViews();
	BuildPSOs();

	ThrowIfFailed(m_commandList->Close());
	ID3D12CommandList* cmdLists[] = { m_commandList.Get() };
	m_commandQueue->ExecuteCommandLists(_countof(cmdLists), cmdLists);

	FlushCommandQueue();

	return true;
}

void Application::OnResize()
{
	Super::OnResize();

	// The window resized, so update the aspect ratio and recompute the projection matrix.
	const XMMATRIX P = XMMatrixPerspectiveFovLH(0.25f * XM_PI, AspectRatio(), 1.0f, 1000.0f);
	XMStoreFloat4x4(&m_proj, P);
}

void Application::Update(const GameTimer& gt)
{
	OnKeyboardInput(gt);
	UpdateCamera(gt);

	m_frameResourceIndex = (m_frameResourceIndex + 1) % NUM_FRAME_RESOURCES;
	m_frameResource = m_frameResources[m_frameResourceIndex].get();

	if (m_frameResource->Fence != 0 && m_fence->GetCompletedValue() < m_frameResource->Fence)
	{
		const HANDLE eventHandle = CreateEventEx(nullptr, nullptr, false, EVENT_ALL_ACCESS);
		assert(eventHandle);
		ThrowIfFailed(m_fence->SetEventOnCompletion(m_frameResource->Fence, eventHandle));
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}

	UpdateObjectCBs(gt);
	UpdateMainPassCB(gt);
}

void Application::Draw(const GameTimer& gt)
{
	const auto cmdListAlloc = m_frameResource->CmdListAlloc;

	ThrowIfFailed(cmdListAlloc->Reset());

	ThrowIfFailed(m_commandList->Reset(cmdListAlloc.Get(), m_PSOs["opaque_wireframe"].Get()));

	m_commandList->RSSetViewports(1, &m_screenViewport);
	m_commandList->RSSetScissorRects(1, &m_scissorRect);

	m_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(
		CurrentBackBuffer(),
		D3D12_RESOURCE_STATE_PRESENT,
		D3D12_RESOURCE_STATE_RENDER_TARGET));

	m_commandList->ClearRenderTargetView(CurrentBackBufferView(), DirectX::Colors::LightSteelBlue, 0, nullptr);
	m_commandList->ClearDepthStencilView(DepthStencilView(), D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

	m_commandList->OMSetRenderTargets(1, &CurrentBackBufferView(), true, &DepthStencilView());

	m_commandList->SetGraphicsRootSignature(m_rootSignature.Get());

	m_commandList->SetGraphicsRootConstantBufferView(1, m_frameResource->PassCB->Resource()->GetGPUVirtualAddress());

	DrawRenderItems(m_commandList.Get(), m_opaqueRenderItems);

	m_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(
		CurrentBackBuffer(),
		D3D12_RESOURCE_STATE_RENDER_TARGET,
		D3D12_RESOURCE_STATE_PRESENT));

	ThrowIfFailed(m_commandList->Close());

	ID3D12CommandList* cmdsList[] = { m_commandList.Get() };
	m_commandQueue->ExecuteCommandLists(_countof(cmdsList), cmdsList);

	ThrowIfFailed(m_swapChain->Present(0, 0));
	m_currentBackBuffer = (m_currentBackBuffer + 1) % SWAP_CHAIN_BUFFER_COUNT;

	m_frameResource->Fence = ++m_currentFence;

	m_commandQueue->Signal(m_fence.Get(), m_currentFence);
}

void Application::OnMouseDown(WPARAM btnState, int x, int y)
{
	m_lastMousePos.x = x;
	m_lastMousePos.y = y;

	SetCapture(m_hMainWnd);
}

void Application::OnMouseUp(WPARAM btnState, int x, int y)
{
	ReleaseCapture();
}

void Application::OnMouseMove(WPARAM btnState, int x, int y)
{
	if ((btnState & MK_LBUTTON) != 0)
	{
		// Make each pixel correspond to a quarter of a degree.
		const float dx = XMConvertToRadians(0.25f * static_cast<float>(x - m_lastMousePos.x));
		const float dy = XMConvertToRadians(0.25f * static_cast<float>(y - m_lastMousePos.y));

		// Update angles based on input to orbit camera around box.
		m_theta -= dx;
		m_phi -= dy;

		// Restrict the angle m_phi.
		m_phi = MathHelper::Clamp(m_phi, 0.1f, DirectX::XM_PI - 0.1f);
	}
	else if ((btnState & MK_RBUTTON) != 0)
	{
		// Make each pixel correspond to 0.2 unit in the scene.
		const float dx = 0.05f * static_cast<float>(x - m_lastMousePos.x);
		const float dy = 0.05f * static_cast<float>(y - m_lastMousePos.y);

		// Update the camera radius based on input.
		m_radius += dx - dy;

		// Restrict the radius.
		m_radius = MathHelper::Clamp(m_radius, 5.0f, 150.0f);
	}

	m_lastMousePos.x = x;
	m_lastMousePos.y = y;
}

void Application::OnKeyboardInput(const GameTimer& gt)
{
}

void Application::UpdateCamera(const GameTimer& gt)
{
	// Convert Spherical to Cartesian coordinates.
	m_eyePos.x = m_radius * sinf(m_phi) * cosf(m_theta);
	m_eyePos.y = m_radius * cosf(m_phi);
	m_eyePos.z = m_radius * sinf(m_phi) * sinf(m_theta);

	// Build the view matrix.
	XMVECTOR pos = XMVectorSet(m_eyePos.x, m_eyePos.y, m_eyePos.z, 1.0f);
	XMVECTOR target = XMVectorZero();
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX view = XMMatrixLookAtLH(pos, target, up);
	XMStoreFloat4x4(&m_view, view);
}

void Application::UpdateObjectCBs(const GameTimer& gt) const
{
	const auto currentObjectCB = m_frameResource->ObjectCB.get();
	for (auto& e : m_allRenderItems)
	{
		if (gt.DeltaTime() > 0)
		{
			e->NumFramesDirty = NUM_FRAME_RESOURCES;
		}
		if (e->NumFramesDirty > 0)
		{
			const XMMATRIX world = XMLoadFloat4x4(&e->World);
			const XMMATRIX bones[2] =
			{
				XMMatrixIdentity(),
				XMMatrixRotationZ(XMConvertToRadians(std::sinf(gt.GameTime() * 4.0f) * 45.0f))
			};

			ObjectConstants objConstants;
			XMStoreFloat4x4(&objConstants.World, XMMatrixTranspose(world));
			XMStoreFloat4x4(&objConstants.Bones[0], XMMatrixTranspose(bones[0]));
			XMStoreFloat4x4(&objConstants.Bones[1], XMMatrixTranspose(bones[1]));

			currentObjectCB->CopyData(e->ObjCBIndex, objConstants);

			--(e->NumFramesDirty);
		}
	}
}

void Application::UpdateMainPassCB(const GameTimer& gt)
{
	const XMMATRIX view = XMLoadFloat4x4(&m_view);
	const XMMATRIX proj = XMLoadFloat4x4(&m_proj);

	const XMMATRIX viewProj = XMMatrixMultiply(view, proj);
	
	XMStoreFloat4x4(&m_mainPassCB.ViewProj, XMMatrixTranspose(viewProj));
	m_mainPassCB.TotalTime = gt.GameTime();

	const auto currentPassCB = m_frameResource->PassCB.get();
	currentPassCB->CopyData(0, m_mainPassCB);
}

void Application::BuildDescriptorHeaps()
{
	const UINT objCount = static_cast<UINT>(m_opaqueRenderItems.size());

	const UINT descCount = (objCount + 1) * NUM_FRAME_RESOURCES;

	m_passCbvOffset = objCount * NUM_FRAME_RESOURCES;

	D3D12_DESCRIPTOR_HEAP_DESC heapDesc;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	heapDesc.NodeMask = 0;
	heapDesc.NumDescriptors = descCount;
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;

	ThrowIfFailed(m_device->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(m_cbvHeap.GetAddressOf())));
}

void Application::BuildConstantBufferViews() const
{
	const UINT objCBByteSize = d3dUtil::CalcConstantBufferByteSize(sizeof(ObjectConstants));

	const UINT objCount = static_cast<UINT>(m_opaqueRenderItems.size());

	for (UINT frameIndex = 0; frameIndex < NUM_FRAME_RESOURCES; ++frameIndex)
	{
		const auto objectCB = m_frameResources[frameIndex]->ObjectCB->Resource();
		for (UINT cbIndex = 0; cbIndex < objCount; ++cbIndex)
		{
			auto gpuAddress = objectCB->GetGPUVirtualAddress();
			gpuAddress += objCBByteSize * cbIndex;

			D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc;
			cbvDesc.BufferLocation = gpuAddress;
			cbvDesc.SizeInBytes = objCBByteSize;

			CD3DX12_CPU_DESCRIPTOR_HANDLE handle(m_cbvHeap->GetCPUDescriptorHandleForHeapStart());
			handle.Offset(static_cast<INT>(cbIndex + objCount * frameIndex), m_cbvSrvDescriptorSize);

			m_device->CreateConstantBufferView(&cbvDesc, handle);
		}
	}

	const UINT passCBByteSize = d3dUtil::CalcConstantBufferByteSize(sizeof(PassConstants));

	// Last three descriptors are the pass CBVs for each frame resource.
	for (int frameIndex = 0; frameIndex < NUM_FRAME_RESOURCES; ++frameIndex)
	{
		const auto passCB = m_frameResources[frameIndex]->PassCB->Resource();
		const D3D12_GPU_VIRTUAL_ADDRESS cbAddress = passCB->GetGPUVirtualAddress();

		// Offset to the pass cbv in the descriptor heap.
		const int heapIndex = static_cast<INT>(m_passCbvOffset) + frameIndex;
		auto handle = CD3DX12_CPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetCPUDescriptorHandleForHeapStart());
		handle.Offset(heapIndex, m_cbvSrvDescriptorSize);

		D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc;
		cbvDesc.BufferLocation = cbAddress;
		cbvDesc.SizeInBytes = passCBByteSize;

		m_device->CreateConstantBufferView(&cbvDesc, handle);
	}
}

void Application::BuildRootSignature()
{

	CD3DX12_ROOT_PARAMETER slotRootParameters[3];

	slotRootParameters[0].InitAsConstantBufferView(0);
	slotRootParameters[1].InitAsConstantBufferView(1);
	slotRootParameters[2].InitAsConstantBufferView(2);

	const auto rootSignatureDesc = CD3DX12_ROOT_SIGNATURE_DESC(_countof(slotRootParameters), slotRootParameters, 0, nullptr,
	                                                           D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	ComPtr<ID3DBlob> rootSignatureBlob;
	ComPtr<ID3DBlob> errorBlob;

	const auto hr = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, rootSignatureBlob.GetAddressOf(), errorBlob.GetAddressOf());
	if (errorBlob != nullptr)
	{
		::OutputDebugStringA(static_cast<char*>(errorBlob->GetBufferPointer()));
	}
	ThrowIfFailed(hr);

	if (FAILED(m_device->CreateRootSignature(0,
		rootSignatureBlob->GetBufferPointer(),
		rootSignatureBlob->GetBufferSize(),
		IID_PPV_ARGS(m_rootSignature.GetAddressOf()))))
	{
		ThrowIfFailed(m_device->GetDeviceRemovedReason());
	}
}

void Application::BuildShadersAndInputLayout()
{
	const std::wstring path = L"Shaders\\";
	const std::wstring vertexShaderFileName = L"VertexShader";
	const std::wstring pixelShaderFileName = L"PixelShader";
	const std::wstring compiledExtension = L".cso";
	const std::wstring sourceExtension = L".hlsl";

	m_shaders["standardVS"] = d3dUtil::TryLoadOrCompileShader(path + vertexShaderFileName + compiledExtension, path + vertexShaderFileName + sourceExtension, nullptr, "VS", "vs_5_0");
	m_shaders["opaquePS"] = d3dUtil::TryLoadOrCompileShader(path + pixelShaderFileName + compiledExtension, path + pixelShaderFileName + sourceExtension, nullptr, "PS", "ps_5_0");

	m_inputLayout =
	{
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "WEIGHT", 0, DXGI_FORMAT_R32_FLOAT, 0, 28, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	};
}

void Application::BuildShapeGeometry()
{
	GeometryGenerator geoGen;
	GeometryGenerator::MeshData cylinder = geoGen.CreateCylinder(1.0f, 1.0f, 4.0f, 32, 8);

	//
	// Extract the vertex elements we are interested and apply the height function to
	// each vertex.  In addition, color the vertices based on their height so we have
	// sandy looking beaches, grassy low hills, and snow mountain peaks.
	//

	// Define the SubmeshGeometry that cover different 
	// regions of the vertex/index buffers.

	SubmeshGeometry cylinderSubmesh;
	cylinderSubmesh.IndexCount = static_cast<UINT>(cylinder.Indices32.size());
	cylinderSubmesh.StartIndexLocation = 0;
	cylinderSubmesh.BaseVertexLocation = 0;

	const auto totalVertexCount = cylinder.m_vertices.size();

	std::vector<Vertex> vertices(totalVertexCount);

	for (size_t i = 0; i < cylinder.m_vertices.size(); ++i)
	{
		XMStoreFloat4(&(vertices[i].Color), DirectX::Colors::Red);
		vertices[i].Pos = cylinder.m_vertices[i].Position;
		vertices[i].Weight = cylinder.m_vertices[i].Weight;
	}
	auto geo = std::make_unique<MeshGeometry>();

	std::vector<std::uint32_t> indices;
	indices.insert(indices.end(), std::begin(cylinder.Indices32), std::end(cylinder.Indices32));

	const UINT vbByteSize = static_cast<UINT>(vertices.size()) * sizeof(Vertex);
	const UINT ibByteSize = static_cast<UINT>(indices.size()) * sizeof(std::uint32_t);

	geo->Name = "shapeGeo";

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCpu));
	CopyMemory(geo->VertexBufferCpu->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCpu));
	CopyMemory(geo->IndexBufferCpu->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGpu = d3dUtil::CreateDefaultAndInitUploadBuffers(m_device.Get(),
		m_commandList.Get(), vertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGpu = d3dUtil::CreateDefaultAndInitUploadBuffers(m_device.Get(),
		m_commandList.Get(), indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexBufferByteSize = ibByteSize;

	geo->VertexByteStride = sizeof(Vertex);
	geo->IndexFormat = DXGI_FORMAT_R32_UINT;

	geo->DrawArgs["cylinder"] = cylinderSubmesh;

	m_geometries[geo->Name] = std::move(geo);
}

void Application::BuildPSOs()
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC opaquePsoDesc;

	//
	// PSO for opaque objects.
	//
	ZeroMemory(&opaquePsoDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	opaquePsoDesc.InputLayout = { m_inputLayout.data(), static_cast<UINT>(m_inputLayout.size()) };
	opaquePsoDesc.pRootSignature = m_rootSignature.Get();
	opaquePsoDesc.VS = {
		reinterpret_cast<BYTE*>(m_shaders["standardVS"]->GetBufferPointer()),
		m_shaders["standardVS"]->GetBufferSize()
	};
	opaquePsoDesc.PS = {
		reinterpret_cast<BYTE*>(m_shaders["opaquePS"]->GetBufferPointer()),
		m_shaders["opaquePS"]->GetBufferSize()
	};
	opaquePsoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	opaquePsoDesc.RasterizerState.FillMode = D3D12_FILL_MODE_SOLID;
	opaquePsoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	opaquePsoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	opaquePsoDesc.SampleMask = UINT_MAX;
	opaquePsoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	opaquePsoDesc.NumRenderTargets = 1;
	opaquePsoDesc.RTVFormats[0] = m_backBufferFormat;
	opaquePsoDesc.SampleDesc.Count = Get4xMsaaCount();
	opaquePsoDesc.SampleDesc.Quality = Get4xMsaaQuality();
	opaquePsoDesc.DSVFormat = m_depthStencilFormat;
	ThrowIfFailed(m_device->CreateGraphicsPipelineState(&opaquePsoDesc, IID_PPV_ARGS(&m_PSOs["opaque"])));


	//
	// PSO for opaque wireframe objects.
	//

	D3D12_GRAPHICS_PIPELINE_STATE_DESC opaqueWireframePsoDesc = opaquePsoDesc;
	opaqueWireframePsoDesc.RasterizerState.FillMode = D3D12_FILL_MODE_WIREFRAME;
	ThrowIfFailed(m_device->CreateGraphicsPipelineState(&opaqueWireframePsoDesc, IID_PPV_ARGS(&m_PSOs["opaque_wireframe"])));
}

void Application::BuildFrameResources()
{
	for (int i = 0; i < NUM_FRAME_RESOURCES; ++i)
	{
		m_frameResources.push_back(std::make_unique<FrameResource>(m_device.Get(), 1, static_cast<UINT>(m_allRenderItems.size()), 0));
	}
}

void Application::BuildRenderItems()
{
	UINT objCBIndex = 0;

	auto cylinderRenderItem = std::make_unique<RenderItem>();
	XMStoreFloat4x4(&cylinderRenderItem->World, XMMatrixTranslation(0.0f, -2.0f, 0.0f));
	cylinderRenderItem->ObjCBIndex = objCBIndex++;
	cylinderRenderItem->Geo = m_geometries["shapeGeo"].get();
	cylinderRenderItem->PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	cylinderRenderItem->IndexCount = cylinderRenderItem->Geo->DrawArgs["cylinder"].IndexCount;
	cylinderRenderItem->StartIndexLocation = cylinderRenderItem->Geo->DrawArgs["cylinder"].StartIndexLocation;
	cylinderRenderItem->BaseVertexLocation = cylinderRenderItem->Geo->DrawArgs["cylinder"].BaseVertexLocation;
	m_allRenderItems.push_back(std::move(cylinderRenderItem));

	for (auto& renderItem : m_allRenderItems)
	{
		m_opaqueRenderItems.push_back(renderItem.get());
	}
}

void Application::DrawRenderItems(ID3D12GraphicsCommandList* cmdList, const std::vector<RenderItem*>& renderItems) const
{
	const UINT objCBByteSize = d3dUtil::CalcConstantBufferByteSize(sizeof(ObjectConstants));

	const auto objectCB = m_frameResource->ObjectCB->Resource();

	for (const auto renderItem : renderItems)
	{
		cmdList->IASetVertexBuffers(0, 1, &renderItem->Geo->VertexBufferView());
		cmdList->IASetIndexBuffer(&renderItem->Geo->IndexBufferView());
		cmdList->IASetPrimitiveTopology(renderItem->PrimitiveType);

		const D3D12_GPU_VIRTUAL_ADDRESS objCBAddress = objectCB->GetGPUVirtualAddress() + renderItem->ObjCBIndex * objCBByteSize;
		
		cmdList->SetGraphicsRootConstantBufferView(0, objCBAddress);

		cmdList->DrawIndexedInstanced(renderItem->IndexCount, 1, renderItem->StartIndexLocation, renderItem->BaseVertexLocation, 0);
	}
}
