#pragma once

#include <cstdint>
#include <DirectXMath.h>
#include <vector>

class GeometryGenerator
{
public:

	using uint16 = std::uint16_t;
	using uint32 = std::uint32_t;

	struct Vertex
	{
		Vertex() = default;
		Vertex(
			const DirectX::XMFLOAT3& p,
			const DirectX::XMFLOAT3& n,
			const float& weight) :
			Position(p),
			Weight(weight),
			Normal(n) {}
		Vertex(
			float px, float py, float pz,
			float nx, float ny, float nz,
			float weight) :
			Position(px, py, pz),
			Weight(weight),
			Normal(nx, ny, nz) {}

		DirectX::XMFLOAT3 Position{};
		float Weight{};
		DirectX::XMFLOAT3 Normal{};
	};

	struct MeshData
	{
		std::vector<Vertex> m_vertices{};
		std::vector<uint32> Indices32{};

		std::vector<uint16>& GetIndices16();

	private:
		std::vector<uint16> m_indices16{};
	};

	// Creates a box centered at the origin with the given dimensions, where each
	// face has m rows and n columns of vertices.
	MeshData CreateBox(float width, float height, float depth, uint32 numSubdivisions);

	// Creates a sphere centered at the origin with the given radius. The
	// slices and stacks parameters control the degree of tessellation.
	MeshData CreateSphere(float radius, uint32 sliceCount, uint32 stackCount);

	// Creates a geosphere centered at the origin with the given radius. The
	// depth controls the level of tessellation.
	MeshData CreateGeosphere(float radius, uint32 numSubdivisions);

	// Creates a cylinder parallel to the y-axis, and centered about the origin.  
	// The bottom and top radius can vary to form various cone shapes rather than true
	// cylinders. The slices and stacks parameters control the degree of tessellation.
	static MeshData CreateCylinder(float bottomRadius, float topRadius, float height, uint32 sliceCount, uint32 stackCount);

	// Creates an mxn grid in the xz-plane with m rows and n columns, centered
	// at the origin with the specified width and depth.
	MeshData CreateGrid(float width, float depth, uint32 m, uint32 n);

	// Creates a quad aligned with the screen. This is useful for postprocessing and screen effects.
	MeshData CreateQuad(float x, float y, float w, float h, float depth);

private:
	void Subdivide(MeshData& meshData);
	Vertex MidPoint(const Vertex& v0, const Vertex& v1);
	static void BuildCylinderTopCap(const float topRadius, const float height, const uint32 sliceCount, MeshData& meshData);
	static void BuildCylinderBottomCap(const float bottomRadius, const float height, const uint32 sliceCount, MeshData& meshData);
};

