#include "Common.hlsl"

VertexOut VS(const VertexIn vertexIn)
{
    VertexOut vertexOut;

    // Transform to homogeneous clip space.
    const float4x4 animationMatrix = mul(vertexIn.Weight, g_bone1) + mul(1.0f - vertexIn.Weight, g_bone0);
    const float4x4 finalMatrix = mul(animationMatrix, g_world);
    vertexOut.Position = mul(mul(float4(vertexIn.Position, 1.0f), finalMatrix), g_viewProj);
    vertexOut.Color = vertexIn.Color;

    return vertexOut;
}
