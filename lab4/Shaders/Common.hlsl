cbuffer cbPerObject : register(b0)
{
    float4x4 g_world;
    float4x4 g_bone0;
    float4x4 g_bone1;
}

cbuffer cbPass : register(b1)
{
    float4x4 g_viewProj;
    float g_totalTime;
}

struct VertexIn
{
    float4 Color    : COLOR;
    float3 Position : POSITION;
    float  Weight   : WEIGHT;
};

struct VertexOut
{
    float4 Position : SV_POSITION;
    float4 Color    : COLOR;
};

