#include "GeometryGenerator.h"
#include <DirectXMath.h>

using uint16 = GeometryGenerator::uint16;
using uint32 = GeometryGenerator::uint32;
using MeshData = GeometryGenerator::MeshData;
using Vertex = GeometryGenerator::Vertex;
using namespace DirectX;

namespace 
{
	std::size_t pow(std::size_t value, std::size_t power)
	{
		if (!power)
		{
			return 1;
		}
		std::size_t result = 1;
		while (power > 0)
		{
			if (0 == power % 2)
			{
				power /= 2;
				value *= value;
			}
			else
			{
				--power;
				result *= value;

				power /= 2;
				value *= value;
			}
		}
		return result;
	}
}

MeshData GeometryGenerator::CreateShape(float radius, const uint32 numSubdivisions)
{
	MeshData out;
	const float diagonal = radius / sqrtf(2);
	out.Vertices =
	{
		{ 0.0f, radius, 0.0f, 0.0f, radius, 0.0f },

		{ -diagonal, 0.0f, -diagonal, -diagonal, 0.0f, -diagonal },
		{ -diagonal, 0.0f, +diagonal, -diagonal, 0.0f, +diagonal },
		{ +diagonal, 0.0f, +diagonal, +diagonal, 0.0f, +diagonal },
		{ +diagonal, 0.0f, -diagonal, +diagonal, 0.0f, -diagonal },

		{ 0.0f, -radius, 0.0f, 0.0f, -radius, 0.0f },
	};

	out.Indices32 =
	{
		0, 1, 2,
		0, 2, 3,
		0, 3, 4,
		0, 4, 1,

		5, 1, 4,
		5, 4, 3,
		5, 3, 2,
		5, 2, 1
	};

	out.SubdivideCount = 0;
	out.VerticesTopBound = 6;
	out.IndicesBottomBound = 0;
	out.IndicesTopBound = 24;

	InitializeAllVerticesOfShape(out);



	for (uint32 i = 0; i < numSubdivisions; ++i)
	{
		SubdivideShape(out);
	}

	return out;
}

bool GeometryGenerator::SubdivideShape(MeshData& shape)
{
	bool out = false;
	if (shape.SubdivideCount < MAX_SHAPE_SUBDIVISION_COUNT) {
		out = true;
		
		constexpr uint32 numInitialTriangles = 8;
		constexpr uint32 additionalTriangles = 4;

		const std::size_t numTriangles = numInitialTriangles * pow(additionalTriangles, shape.SubdivideCount);

		shape.VerticesTopBound += numTriangles / 4 * 3;
		++(shape.SubdivideCount);
		shape.IndicesBottomBound = shape.IndicesTopBound;
		shape.IndicesTopBound += numTriangles * additionalTriangles * 3;
	}
	return out;
}

bool GeometryGenerator::SimplifyShape(MeshData& shape)
{
	bool out = false;
	if (shape.SubdivideCount > 0) {
		out = true;
		
		constexpr std::size_t numInitialTriangles = 8;
		constexpr std::size_t additionalTriangles = 4;
		
		std::size_t numTriangles = numInitialTriangles * pow(additionalTriangles, shape.SubdivideCount);

		shape.VerticesTopBound -= numTriangles / 4 * 3;
		--(shape.SubdivideCount);
		numTriangles /= additionalTriangles;
		shape.IndicesTopBound = shape.IndicesBottomBound;
		shape.IndicesBottomBound -= numTriangles * 3;
	}
	return out;
}

void GeometryGenerator::InitializeAllVerticesOfShape(MeshData& shape)
{
	while (shape.SubdivideCount < MAX_SHAPE_SUBDIVISION_COUNT) {
		// Save a copy of the input geometry.
		const MeshData inputCopy = shape;

		//       v1
		//       *
		//      / \
		//     /   \
		//  m0*-----*m1
		//   / \   / \
		//  /   \ /   \
		// *-----*-----*
		// v0    m2     v2
		
		const auto numVertices = inputCopy.Vertices.size();
		const auto v = inputCopy.Vertices[0].PyramidPos;
		const float radius = sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
		std::size_t shift = 0;
		std::size_t step = 8;
		for (std::size_t i = 0; i < shape.SubdivideCount; ++i)
		{
			shift += step;
			step *= 4;
		}
		const std::size_t trianglesCount = step;
		for (std::size_t i = 0; i < trianglesCount; ++i)
		{
			Vertex v0 = inputCopy.Vertices[inputCopy.Indices32[(shift + i) * 3u + 0]];
			Vertex v1 = inputCopy.Vertices[inputCopy.Indices32[(shift + i) * 3u + 1]];
			Vertex v2 = inputCopy.Vertices[inputCopy.Indices32[(shift + i) * 3u + 2]];

			//
			// Generate the midpoints.
			//

			Vertex m0 = MidPoint(v0, v1, radius);
			Vertex m1 = MidPoint(v1, v2, radius);
			Vertex m2 = MidPoint(v0, v2, radius);

			//
			// Add new geometry.
			//

			// ReSharper disable CppInconsistentNaming
			uint32 v0_id = inputCopy.Indices32[(shift + i) * 3u + 0];
			uint32 v1_id = inputCopy.Indices32[(shift + i) * 3u + 1];
			uint32 v2_id = inputCopy.Indices32[(shift + i) * 3u + 2];
			uint32 m0_id = static_cast<uint32>(numVertices + i * 3u) + 0;
			uint32 m1_id = static_cast<uint32>(numVertices + i * 3u) + 1;
			uint32 m2_id = static_cast<uint32>(numVertices + i * 3u) + 2;
			// ReSharper restore CppInconsistentNaming

			shape.Vertices.push_back(m0);
			shape.Vertices.push_back(m1);
			shape.Vertices.push_back(m2);

			shape.Indices32.push_back(v0_id);
			shape.Indices32.push_back(m0_id);
			shape.Indices32.push_back(m2_id);

			shape.Indices32.push_back(m0_id);
			shape.Indices32.push_back(m1_id);
			shape.Indices32.push_back(m2_id);

			shape.Indices32.push_back(m2_id);
			shape.Indices32.push_back(m1_id);
			shape.Indices32.push_back(v2_id);

			shape.Indices32.push_back(m0_id);
			shape.Indices32.push_back(v1_id);
			shape.Indices32.push_back(m1_id);
		}
		shape.VerticesTopBound = shape.Vertices.size();
		shape.IndicesBottomBound = shape.IndicesTopBound;
		shape.IndicesTopBound = shape.Indices32.size();

		++(shape.SubdivideCount);
	}
}

Vertex GeometryGenerator::MidPoint(const Vertex& v0, const Vertex& v1, float radius)
{
	const XMVECTOR p0 = XMLoadFloat3(&v0.PyramidPos);
	const XMVECTOR p1 = XMLoadFloat3(&v1.PyramidPos);

	// Compute the midpoints of all the attributes.  Vectors need to be normalized
	// since linear interpolating can make them not unit length.  
	const XMVECTOR pyrPos = 0.5f * (p0 + p1);
	const XMVECTOR sphPos = XMVector3Normalize(p0 + p1) * radius;

	Vertex v;
	XMStoreFloat3(&v.PyramidPos, pyrPos);
	XMStoreFloat3(&v.SpherePos, sphPos);

	return v;
}

