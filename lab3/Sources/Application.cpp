#include "Application.h"
#include <DirectXColors.h>
#include <d3dcompiler.h>

using namespace DirectX;

namespace
{
	void FillMeshGeometryWithMeshData(
		const GeometryGenerator::MeshData& shape,
		MeshGeometry* geo,
		const ComPtr<ID3D12Device>& device,
		const ComPtr<ID3D12GraphicsCommandList>& commandList)
	{
		SubmeshGeometry submesh;
		submesh.BaseVertexLocation = 0;
		ThrowIfNullptr(geo);
		ThrowIfNullptr(device.GetAddressOf());
		ThrowIfNullptr(commandList.GetAddressOf());


		const auto totalVertexCount = shape.Vertices.size();

		std::vector<Vertex> vertices(totalVertexCount);

		for (size_t i = 0; i < shape.Vertices.size(); ++i)
		{
			vertices[i].Pos = shape.Vertices[i].PyramidPos;
			vertices[i].SecondAnimationPos = shape.Vertices[i].SpherePos;
			vertices[i].Color = XMFLOAT4(Colors::DarkGreen);
		}
		


		uint32_t beginning = 0;
		uint32_t triangleCount = 8;
		for (uint32_t i = 0; i < shape.SubdivideCount; ++i)
		{
			beginning += triangleCount;
			triangleCount *= 4;
		}
		const auto start = std::begin(shape.Indices32) + beginning * 3;
		const auto end = std::begin(shape.Indices32) + (beginning + triangleCount) * 3;

		submesh.IndexCount = static_cast<UINT>(shape.IndicesTopBound);
		submesh.StartIndexLocation = static_cast<UINT>(shape.IndicesBottomBound);

		const UINT vertexBufferByteSize = static_cast<UINT>(vertices.size()) * sizeof(Vertex);
		const UINT indexBufferByteSize = static_cast<UINT>(shape.Indices32.size()) * sizeof(std::uint32_t);

		geo->Name = "shape";

		ThrowIfFailed(D3DCreateBlob(vertexBufferByteSize, &geo->VertexBufferCpu));
		CopyMemory(geo->VertexBufferCpu->GetBufferPointer(), vertices.data(), vertexBufferByteSize);

		ThrowIfFailed(D3DCreateBlob(indexBufferByteSize, &geo->IndexBufferCpu));
		CopyMemory(geo->IndexBufferCpu->GetBufferPointer(), shape.Indices32.data(), indexBufferByteSize);

		geo->VertexBufferGpu = d3dUtil::CreateDefaultAndInitUploadBuffers(device.Get(),
			commandList.Get(), vertices.data(), vertexBufferByteSize, geo->VertexBufferUploader);

		geo->IndexBufferGpu = d3dUtil::CreateDefaultAndInitUploadBuffers(device.Get(),
			commandList.Get(), shape.Indices32.data(), indexBufferByteSize, geo->IndexBufferUploader);

		geo->VertexBufferByteSize = vertexBufferByteSize;
		geo->IndexBufferByteSize = indexBufferByteSize;
		geo->IndexFormat = DXGI_FORMAT_R32_UINT;


		geo->VertexByteStride = sizeof(Vertex);

		geo->DrawArgs["shape"] = submesh;
	}
}

Application::Application(HINSTANCE hInstance) : Super(hInstance)
{
}

Application::~Application()
{
	if (m_device != nullptr)
		FlushCommandQueue();
}

bool Application::Initialize()
{
	if (!Super::Initialize())
		return false;

	ThrowIfFailed(m_commandList->Reset(m_directCmdListAlloc.Get(), nullptr));

	BuildRootSignature();
	BuildShadersAndInputLayout();
	BuildShapeGeometry();
	BuildRenderItems();
	BuildFrameResources();
	BuildDescriptorHeaps();
	BuildConstantBufferViews();
	BuildPSOs();

	ThrowIfFailed(m_commandList->Close());
	ID3D12CommandList* commandLists[] = { m_commandList.Get() };
	m_commandQueue->ExecuteCommandLists(_countof(commandLists), commandLists);

	FlushCommandQueue();

	return true;
}

void Application::OnResize()
{
	Super::OnResize();

	// The window resized, so update the aspect ratio and recompute the projection matrix.
	XMMATRIX P = XMMatrixPerspectiveFovLH(0.25f * XM_PI, AspectRatio(), 1.0f, 1000.0f);
	XMStoreFloat4x4(&m_proj, P);
}

void Application::Update(const GameTimer& gt)
{
	OnKeyboardInput(gt);
	UpdateCamera(gt);

	if (m_frameResource->Fence != 0 && m_fence->GetCompletedValue() < m_frameResource->Fence)
	{
		const HANDLE eventHandle = CreateEventEx(nullptr, nullptr, false, EVENT_ALL_ACCESS);
		ThrowIfNullptr(eventHandle);
		ThrowIfFailed(m_fence->SetEventOnCompletion(m_frameResource->Fence, eventHandle));
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}

	UpdateObjectCBs(gt);
	UpdateMainPassCB(gt);
}

void Application::Draw(const GameTimer& gt)
{
	const auto commandListAlloc = m_frameResource->CmdListAlloc;

	ThrowIfFailed(commandListAlloc->Reset());

	if (m_isWireframe)
	{
		ThrowIfFailed(m_commandList->Reset(commandListAlloc.Get(), m_PSOs["opaque_wireframe"].Get()));
	}
	else
	{
		ThrowIfFailed(m_commandList->Reset(commandListAlloc.Get(), m_PSOs["opaque"].Get()));
	}

	m_commandList->RSSetViewports(1, &m_screenViewport);
	m_commandList->RSSetScissorRects(1, &m_scissorRect);

	const auto presentToRenderTargetTransition = CD3DX12_RESOURCE_BARRIER::Transition(
		CurrentBackBuffer(),
		D3D12_RESOURCE_STATE_PRESENT,
		D3D12_RESOURCE_STATE_RENDER_TARGET);

	const auto renderTargetToPresentTransition = CD3DX12_RESOURCE_BARRIER::Transition(
		CurrentBackBuffer(),
		D3D12_RESOURCE_STATE_RENDER_TARGET,
		D3D12_RESOURCE_STATE_PRESENT);

	m_commandList->ResourceBarrier(1, &presentToRenderTargetTransition);

	const auto currentBackBufferView = CurrentBackBufferView();

	const auto depthStencilView = DepthStencilView();

	m_commandList->ClearRenderTargetView(currentBackBufferView, Colors::LightSteelBlue, 0, nullptr);
	m_commandList->ClearDepthStencilView(DepthStencilView(), D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);

	m_commandList->OMSetRenderTargets(1, &currentBackBufferView, true, &depthStencilView);

	ID3D12DescriptorHeap* descHeaps[] = { m_cbvHeap.Get() };
	m_commandList->SetDescriptorHeaps(_countof(descHeaps), descHeaps);

	m_commandList->SetGraphicsRootSignature(m_rootSignature.Get());

	CD3DX12_GPU_DESCRIPTOR_HANDLE descHandle(m_cbvHeap->GetGPUDescriptorHandleForHeapStart());
	descHandle.Offset(static_cast<INT>(m_passCbvOffset), m_cbvSrvDescriptorSize);
	m_commandList->SetGraphicsRootDescriptorTable(1, descHandle);

	UpdateQuality();

	DrawRenderItems(m_commandList.Get(), m_opaqueRenderItems);

	m_commandList->ResourceBarrier(1, &renderTargetToPresentTransition);

	ThrowIfFailed(m_commandList->Close());

	ID3D12CommandList* commandsList[] = { m_commandList.Get() };
	m_commandQueue->ExecuteCommandLists(static_cast<UINT>(std::size(commandsList)), commandsList);

	ThrowIfFailed(m_swapChain->Present(0, 0));
	m_currentBackBuffer = (m_currentBackBuffer + 1) % SWAP_CHAIN_BUFFER_COUNT;

	m_frameResource->Fence = ++m_currentFence;

	m_commandQueue->Signal(m_fence.Get(), m_currentFence);

	FlushCommandQueue();
}

void Application::OnMouseDown(WPARAM btnState, int x, int y)
{
	m_lastMousePos.x = x;
	m_lastMousePos.y = y;

	SetCapture(m_hMainWnd);
}

void Application::OnMouseUp(WPARAM btnState, int x, int y)
{
	ReleaseCapture();
}

void Application::OnMouseMove(WPARAM btnState, int x, int y)
{
	if ((btnState & MK_LBUTTON) != 0)
	{
		// Make each pixel correspond to a quarter of a degree.
		const float dx = XMConvertToRadians(0.25f * static_cast<float>(x - m_lastMousePos.x));
		const float dy = XMConvertToRadians(0.25f * static_cast<float>(y - m_lastMousePos.y));

		// Update angles based on input to orbit camera around box.
		m_theta -= dx;
		m_phi -= dy;

		// Restrict the angle m_phi.
		m_phi = MathHelper::Clamp(m_phi, 0.1f, DirectX::XM_PI - 0.1f);
	}
	else if ((btnState & MK_RBUTTON) != 0)
	{
		// Make each pixel correspond to 0.2 unit in the scene.
		const float dx = 0.05f * static_cast<float>(x - m_lastMousePos.x);
		const float dy = 0.05f * static_cast<float>(y - m_lastMousePos.y);

		// Update the camera radius based on input.
		m_radius += dx - dy;

		// Restrict the radius.
		m_radius = MathHelper::Clamp(m_radius, 5.0f, 150.0f);
	}

	m_lastMousePos.x = x;
	m_lastMousePos.y = y;
}

void Application::OnKeyboardInput(const GameTimer& gt)
{
	if (GetAsyncKeyState(VK_LEFT) & 1 || GetAsyncKeyState(VK_DOWN) & 1)
	{
		m_changeShapeDetails = -1;
	}

	if (GetAsyncKeyState(VK_RIGHT) & 1 || GetAsyncKeyState(VK_UP) & 1)
	{
		m_changeShapeDetails = 1;
	}

	m_isWireframe = GetAsyncKeyState('1') & 0x8000;
}

void Application::UpdateCamera(const GameTimer& gt)
{
	// Convert Spherical to Cartesian coordinates.
	m_eyePos.x = m_radius * sinf(m_phi) * cosf(m_theta);
	m_eyePos.y = m_radius * cosf(m_phi);
	m_eyePos.z = m_radius * sinf(m_phi) * sinf(m_theta);

	// Build the view matrix.
	const XMVECTOR pos = XMVectorSet(m_eyePos.x, m_eyePos.y, m_eyePos.z, 1.0f);
	const XMVECTOR target = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	const XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	const XMMATRIX view = XMMatrixLookAtLH(pos, target, up);
	XMStoreFloat4x4(&m_view, view);
}

void Application::UpdateObjectCBs(const GameTimer& gt) const
{
	const auto currentObjectCB = m_frameResource->ObjectCB.get();
	for (auto& e : m_allRenderItems)
	{
		if (e->NumFramesDirty > 0)
		{
			const XMMATRIX world = XMLoadFloat4x4(&e->World);

			ObjectConstants objConstants;
			XMStoreFloat4x4(&objConstants.World, XMMatrixTranspose(world));

			currentObjectCB->CopyData(e->ObjCBIndex, objConstants);

			--(e->NumFramesDirty);
		}
	}

}

void Application::UpdateMainPassCB(const GameTimer& gt)
{
	const XMMATRIX view = XMLoadFloat4x4(&m_view);
	const XMMATRIX proj = XMLoadFloat4x4(&m_proj);

	const XMMATRIX viewProj = XMMatrixMultiply(view, proj);
	
	XMStoreFloat4x4(&m_mainPassCB.ViewProj, XMMatrixTranspose(viewProj));
	m_mainPassCB.TotalTime = gt.GameTime();

	const auto currentPassCB = m_frameResource->PassCB.get();
	currentPassCB->CopyData(0, m_mainPassCB);
}

void Application::UpdateQuality()
{
	bool updateRenderItems = false;
	if (m_changeShapeDetails > 0)
	{
		GeometryGenerator geoGen;
		if (geoGen.SubdivideShape(m_shape))
		{
			auto* geo = m_geometries["shape"].get();
			geo->DrawArgs["shape"].IndexCount = static_cast<UINT>(m_shape.IndicesTopBound - m_shape.IndicesBottomBound);
			geo->DrawArgs["shape"].StartIndexLocation = static_cast<UINT>(m_shape.IndicesBottomBound);
			updateRenderItems = true;
		}
		--m_changeShapeDetails;
	}
	if (m_changeShapeDetails < 0)
	{
		GeometryGenerator geoGen;
		if (geoGen.SimplifyShape(m_shape))
		{
			auto* geo = m_geometries["shape"].get();
			geo->DrawArgs["shape"].IndexCount = static_cast<UINT>(m_shape.IndicesTopBound - m_shape.IndicesBottomBound);
			geo->DrawArgs["shape"].StartIndexLocation = static_cast<UINT>(m_shape.IndicesBottomBound);
			updateRenderItems = true;
		}
		++m_changeShapeDetails;
	}
	if (updateRenderItems)
	{
		m_allRenderItems.clear();
		m_opaqueRenderItems.clear();
		BuildRenderItems();
	}
}

void Application::BuildDescriptorHeaps()
{
	const UINT objCount = static_cast<UINT>(m_opaqueRenderItems.size());

	const UINT descCount = objCount + 1;

	m_passCbvOffset = objCount;

	D3D12_DESCRIPTOR_HEAP_DESC heapDesc;
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	heapDesc.NodeMask = 0;
	heapDesc.NumDescriptors = descCount;
	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;

	ThrowIfFailed(m_device->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(m_cbvHeap.GetAddressOf())));
}

void Application::BuildConstantBufferViews() const
{
	const UINT objCBByteSize = d3dUtil::CalcConstantBufferByteSize(sizeof(ObjectConstants));

	const UINT objCount = static_cast<UINT>(m_opaqueRenderItems.size());

	const auto objectCB = m_frameResource->ObjectCB->Resource();
	for (UINT cbIndex = 0; cbIndex < objCount; ++cbIndex)
	{
		auto gpuAddress = objectCB->GetGPUVirtualAddress();
		gpuAddress += static_cast<D3D12_GPU_VIRTUAL_ADDRESS>(cbIndex) * objCBByteSize;

		D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc;
		cbvDesc.BufferLocation = gpuAddress;
		cbvDesc.SizeInBytes = objCBByteSize;

		CD3DX12_CPU_DESCRIPTOR_HANDLE handle(m_cbvHeap->GetCPUDescriptorHandleForHeapStart());
		handle.Offset(static_cast<INT>(cbIndex), m_cbvSrvDescriptorSize);

		m_device->CreateConstantBufferView(&cbvDesc, handle);
	}

	const UINT passCBByteSize = d3dUtil::CalcConstantBufferByteSize(sizeof(PassConstants));


	const auto passCB = m_frameResource->PassCB->Resource();
	const D3D12_GPU_VIRTUAL_ADDRESS cbAddress = passCB->GetGPUVirtualAddress();

	// Offset to the pass cbv in the descriptor heap.
	const INT heapIndex = static_cast<INT>(m_passCbvOffset);
	auto handle = CD3DX12_CPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetCPUDescriptorHandleForHeapStart());
	handle.Offset(heapIndex, m_cbvSrvDescriptorSize);

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc;
	cbvDesc.BufferLocation = cbAddress;
	cbvDesc.SizeInBytes = passCBByteSize;

	m_device->CreateConstantBufferView(&cbvDesc, handle);
}

void Application::BuildRootSignature()
{
	CD3DX12_DESCRIPTOR_RANGE cbvTable[2];

	cbvTable[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	cbvTable[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);

	CD3DX12_ROOT_PARAMETER slotRootParameters[2];

	slotRootParameters[0].InitAsDescriptorTable(1, &cbvTable[0]);
	slotRootParameters[1].InitAsDescriptorTable(1, &cbvTable[1]);

	const auto rootSignatureDesc = CD3DX12_ROOT_SIGNATURE_DESC(2, slotRootParameters, 0, nullptr,
	                                                           D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	ComPtr<ID3DBlob> rootSignatureBlob;
	ComPtr<ID3DBlob> errorBlob;

	const auto hr = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, rootSignatureBlob.GetAddressOf(), errorBlob.GetAddressOf());
	if (errorBlob != nullptr)
	{
		::OutputDebugStringA(static_cast<char*>(errorBlob->GetBufferPointer()));
	}
	ThrowIfFailed(hr);

	if (FAILED(m_device->CreateRootSignature(0,
		rootSignatureBlob->GetBufferPointer(),
		rootSignatureBlob->GetBufferSize(),
		IID_PPV_ARGS(m_rootSignature.GetAddressOf()))))
	{
		ThrowIfFailed(m_device->GetDeviceRemovedReason());
	}
}

void Application::BuildShadersAndInputLayout()
{
	const std::wstring path = L"Shaders\\";
	const std::wstring vertexShaderFileName = L"VertexShader";
	const std::wstring pixelShaderFileName = L"PixelShader";
	const std::wstring compiledExtension = L".cso";
	const std::wstring sourceExtension = L".hlsl";

	m_shaders["standardVS"] = d3dUtil::TryLoadOrCompileShader(path + vertexShaderFileName + compiledExtension, path + vertexShaderFileName + sourceExtension, nullptr, "VS", "vs_5_0");
	m_shaders["opaquePS"] = d3dUtil::TryLoadOrCompileShader(path + pixelShaderFileName + compiledExtension, path + pixelShaderFileName + sourceExtension, nullptr, "PS", "ps_5_0");

	m_inputLayout =
	{
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "OCTAHEDRON_POS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "WEIGHT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 28, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "SPHERE_POS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	};
}

void Application::BuildShapeGeometry()
{
	GeometryGenerator geoGen;
	m_shape = geoGen.CreateShape(1, 0);


	// Define the SubmeshGeometry that cover different 
	// regions of the vertex/index buffers.

	//
	// Extract the vertex elements we are interested in and pack the
	// vertices of all the meshes into one vertex buffer.
	//

	auto geo = std::make_unique<MeshGeometry>();

	FillMeshGeometryWithMeshData(m_shape, geo.get(), m_device.Get(), m_commandList.Get());

	m_geometries[geo->Name] = std::move(geo);
}

void Application::BuildPSOs()
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC opaquePsoDesc;

	//
	// PSO for opaque objects.
	//
	ZeroMemory(&opaquePsoDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	opaquePsoDesc.InputLayout = { m_inputLayout.data(), (UINT)m_inputLayout.size() };
	opaquePsoDesc.pRootSignature = m_rootSignature.Get();
	opaquePsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_shaders["standardVS"]->GetBufferPointer()),
		m_shaders["standardVS"]->GetBufferSize()
	};
	opaquePsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_shaders["opaquePS"]->GetBufferPointer()),
		m_shaders["opaquePS"]->GetBufferSize()
	};
	opaquePsoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	opaquePsoDesc.RasterizerState.FillMode = D3D12_FILL_MODE_WIREFRAME;
	opaquePsoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	opaquePsoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	opaquePsoDesc.SampleMask = UINT_MAX;
	opaquePsoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	opaquePsoDesc.NumRenderTargets = 1;
	opaquePsoDesc.RTVFormats[0] = m_backBufferFormat;
	opaquePsoDesc.SampleDesc.Count = Get4xMsaaCount();
	opaquePsoDesc.SampleDesc.Quality = Get4xMsaaQuality();
	opaquePsoDesc.DSVFormat = m_depthStencilFormat;
	ThrowIfFailed(m_device->CreateGraphicsPipelineState(&opaquePsoDesc, IID_PPV_ARGS(&m_PSOs["opaque"])));


	//
	// PSO for opaque wireframe objects.
	//

	D3D12_GRAPHICS_PIPELINE_STATE_DESC opaqueWireframePsoDesc = opaquePsoDesc;
	opaqueWireframePsoDesc.RasterizerState.FillMode = m_isWireframe ? D3D12_FILL_MODE_WIREFRAME : D3D12_FILL_MODE_SOLID;
	ThrowIfFailed(m_device->CreateGraphicsPipelineState(&opaqueWireframePsoDesc, IID_PPV_ARGS(&m_PSOs["opaque_wireframe"])));
}

void Application::BuildFrameResources()
{
	m_frameResource = std::make_unique<FrameResource>(m_device.Get(), 1, static_cast<UINT>(m_allRenderItems.size()), 0);
}

void Application::BuildRenderItems()
{
	auto renderItem = std::make_unique<RenderItem>();
	XMStoreFloat4x4(&renderItem->World, XMMatrixScaling(2.0f, 2.0f, 2.0f));
	renderItem->ObjCBIndex = 0;
	renderItem->Geo = m_geometries["shape"].get();
	renderItem->PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	renderItem->IndexCount = renderItem->Geo->DrawArgs["shape"].IndexCount;
	renderItem->StartIndexLocation = renderItem->Geo->DrawArgs["shape"].StartIndexLocation;
	renderItem->BaseVertexLocation = renderItem->Geo->DrawArgs["shape"].BaseVertexLocation;
	m_allRenderItems.push_back(std::move(renderItem));

	// All the render items are opaque.
	for (auto& e : m_allRenderItems)
	{
		m_opaqueRenderItems.push_back(e.get());
	}
}

void Application::DrawRenderItems(ID3D12GraphicsCommandList* cmdList, const std::vector<RenderItem*>& renderItems) const
{
	ThrowIfNullptr(cmdList);
	for (const auto renderItem : renderItems)
	{
		constexpr UINT startSlot = 0;
		constexpr UINT numViews = 1;
		constexpr UINT rootParameterIndex = 0;
		constexpr UINT instanceCount = 1;
		constexpr UINT startInstanceLocation = 0;

		const auto vertexBufferView = renderItem->Geo->VertexBufferView();
		const auto indexBufferView = renderItem->Geo->IndexBufferView();

		cmdList->IASetVertexBuffers(startSlot, numViews, &vertexBufferView);
		cmdList->IASetIndexBuffer(&indexBufferView);
		cmdList->IASetPrimitiveTopology(renderItem->PrimitiveType);

		// Offset to the CBV in the descriptor heap for this object and for this frame resource.
		const UINT cbvIndex = renderItem->ObjCBIndex;
		auto cbvHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(m_cbvHeap->GetGPUDescriptorHandleForHeapStart());
		cbvHandle.Offset(cbvIndex, m_cbvSrvDescriptorSize);

		cmdList->SetGraphicsRootDescriptorTable(rootParameterIndex, cbvHandle);

		cmdList->DrawIndexedInstanced(renderItem->IndexCount, instanceCount, renderItem->StartIndexLocation, renderItem->BaseVertexLocation, startInstanceLocation);
	}
}
