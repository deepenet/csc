#pragma once
#include "D3DApp.h"
#include "MathHelper.h"
#include "d3dUtils.h"
#include "FrameResource.h"
#include "GeometryGenerator.h"
#include <memory>
#include <d3d12.h>

// Lightweight structure stores parameters to draw a shape.
struct RenderItem
{
	RenderItem() = default;

	DirectX::XMFLOAT4X4 World = MathHelper::Identity4x4();

	int NumFramesDirty = 1;

	// Index into GPU constant buffer corresponding to the ObjectCB for this render item.
	UINT ObjCBIndex = -1;

	MeshGeometry* Geo = nullptr;

	// Primitive topology.
	D3D12_PRIMITIVE_TOPOLOGY PrimitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	// DrawIndexedInstanced parameters.
	UINT IndexCount = 0;
	UINT StartIndexLocation = 0;
	int BaseVertexLocation = 0;
};

class Application final : public D3DApp
{
	using Super = D3DApp;

public:
	explicit Application(HINSTANCE hInstance);
	Application(const Application& rhs) = delete;
	Application(Application&& rhs) = delete;
	Application& operator=(const Application& rhs) = delete;
	Application& operator=(Application&& rhs) = delete;
	~Application() override;

	bool Initialize()override;

private:
	void OnResize()override;
	void Update(const GameTimer& gt) override;
	void Draw(const GameTimer& gt) override;

	void OnMouseDown(WPARAM btnState, int x, int y) override;
	void OnMouseUp(WPARAM btnState, int x, int y) override;
	void OnMouseMove(WPARAM btnState, int x, int y) override;

	void OnKeyboardInput(const GameTimer& gt);
	void UpdateCamera(const GameTimer& gt);
	void UpdateObjectCBs(const GameTimer& gt) const;
	void UpdateMainPassCB(const GameTimer& gt);
	void UpdateQuality();

	void BuildDescriptorHeaps();
	void BuildConstantBufferViews() const;
	void BuildRootSignature();
	void BuildShadersAndInputLayout();
	void BuildShapeGeometry();
	void BuildPSOs();
	void BuildFrameResources();
	void BuildRenderItems();
	void DrawRenderItems(ID3D12GraphicsCommandList* cmdList, const std::vector<RenderItem*>& renderItems) const;
 
private:

    std::unique_ptr<FrameResource> m_frameResource{};

    ComPtr<ID3D12RootSignature> m_rootSignature = nullptr;
    ComPtr<ID3D12DescriptorHeap> m_cbvHeap = nullptr;

	ComPtr<ID3D12DescriptorHeap> m_srvDescriptorHeap = nullptr;

	GeometryGenerator::MeshData m_shape;
	std::unordered_map<std::string, std::unique_ptr<MeshGeometry>> m_geometries{};
	std::unordered_map<std::string, ComPtr<ID3DBlob>> m_shaders{};
    std::unordered_map<std::string, ComPtr<ID3D12PipelineState>> m_PSOs{};

    std::vector<D3D12_INPUT_ELEMENT_DESC> m_inputLayout{};

	// List of all the render items.
	std::vector<std::unique_ptr<RenderItem>> m_allRenderItems{};

	// Render items divided by PSO.
	std::vector<RenderItem*> m_opaqueRenderItems{};

    PassConstants m_mainPassCB;

    UINT m_passCbvOffset = 0;

    bool m_isWireframe = false;

	int m_changeShapeDetails = 0;

	bool m_tmp = true;

	DirectX::XMFLOAT3 m_eyePos = { 0.0f, 0.0f, 0.0f };
	DirectX::XMFLOAT4X4 m_view = MathHelper::Identity4x4();
	DirectX::XMFLOAT4X4 m_proj = MathHelper::Identity4x4();

    float m_theta = 1.5f* DirectX::XM_PI;
    float m_phi = 0.2f* DirectX::XM_PI;
    float m_radius = 15.0f;

	POINT m_lastMousePos = { 0, 0 };
};

