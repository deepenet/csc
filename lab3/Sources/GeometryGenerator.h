#pragma once

#include <cstdint>
#include <DirectXMath.h>
#include <vector>

class GeometryGenerator
{
public:

	using uint16 = std::uint16_t;
	using uint32 = std::uint32_t;

	struct Vertex
	{
		Vertex() {}
		Vertex(
			const DirectX::XMFLOAT3& p,
			const DirectX::XMFLOAT3& s) :
			PyramidPos(p),
			SpherePos(s) {}
		Vertex(
			float px, float py, float pz,
			float sx, float sy, float sz) :
			PyramidPos(px, py, pz),
			SpherePos(sx, sy, sz) {}

		DirectX::XMFLOAT3 PyramidPos{};
		DirectX::XMFLOAT3 SpherePos{};
	};

	struct MeshData
	{
		std::vector<Vertex> Vertices{};
		std::vector<uint32> Indices32{};
		std::size_t SubdivideCount = 0;
		std::size_t VerticesTopBound = 0;
		std::size_t IndicesBottomBound = 0;
		std::size_t IndicesTopBound = 0;
	};

	// Creates a box centered at the origin with the given dimensions, where each
	// face has m rows and n columns of vertices.
	static MeshData CreateShape(float radius, uint32 numSubdivisions);
	static bool SubdivideShape(MeshData& shape);
	static bool SimplifyShape(MeshData& shape);

	static constexpr int MAX_SHAPE_SUBDIVISION_COUNT = 8;

private:
	static void InitializeAllVerticesOfShape(MeshData& shape);
	static Vertex MidPoint(const Vertex& v0, const Vertex& v1, float radius);
};

