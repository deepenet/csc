#include "Common.hlsl"

VertexOut VS(const VertexIn vertexIn)
{
    VertexOut vertexOut;

    const float alpha = (sin(g_totalTime * 2) + 1) / 2;
    const float3 vertexPosition = alpha * vertexIn.OctahedronPosition + (1 - alpha) * vertexIn.SpherePosition;

    // Transform to homogeneous clip space.
    const float4 worldPosition = mul(float4(vertexPosition, 1.0f), g_world);
    vertexOut.Position = mul(worldPosition, g_viewProj);

    // Just pass vertex color into the pixel shader.
    vertexOut.Color = vertexIn.Color;

    return vertexOut;
}
