cbuffer cbPerObject : register(b0)
{
    float4x4 g_world;
}

cbuffer cbPass : register(b1)
{
    float4x4 g_viewProj;
    float g_totalTime;
}

struct VertexIn
{
    float4 Color                : COLOR;
    float3 OctahedronPosition   : OCTAHEDRON_POS;
    float  Weight               : WEIGHT;
    float3 SpherePosition       : SPHERE_POS;
};

struct VertexOut
{
    float4 Position : SV_POSITION;
    float4 Color    : COLOR;
};

