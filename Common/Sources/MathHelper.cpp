#include "MathHelper.h"

using DirectX::XMFLOAT4X4;

XMFLOAT4X4 MathHelper::Identity4x4()
{
    static XMFLOAT4X4 I(
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f, 
        0.0f, 0.0f, 1.0f, 0.0f, 
        0.0f, 0.0f, 0.0f, 1.0f
    );

    return I;
}
