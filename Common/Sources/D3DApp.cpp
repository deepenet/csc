#include "D3DApp.h"
#include <windowsx.h>
#include <dxgi.h>
#include <dxgi1_4.h>
#include <cassert>
#include <cassert>
#include <string>
#include <vector>

#include "d3dUtils.h"
#include "d3dx12.h"

constexpr UINT DEFAULT_U_TYPE = 0;

namespace {
    LRESULT CALLBACK
        MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
    {
        // Forward hwnd on because we can get messages (e.g., WM_CREATE)
        // before CreateWindow returns, and thus before mhMainWnd is valid.
        return D3DApp::GetApp()->MsgProc(hwnd, msg, wParam, lParam);
    }

    D3DApp* g_theApp = nullptr;
}

D3DApp::D3DApp(HINSTANCE hInstance) :
    m_hAppInst(hInstance)
{
    assert(g_theApp == nullptr);
    g_theApp = this;
}

bool D3DApp::Initialize()
{
    if (!InitializeMainWindow()) {
        return false;
    }

    try
    {
        InitializeDirect3D();
    }
    catch (HrException&) 
    {
        MessageBox(nullptr, L"InitializeDirect3D Failed.\n", nullptr, DEFAULT_U_TYPE);
        return false;
    }

    OnResize();

    return true;
}

int D3DApp::Run()
{
    MSG msg = { nullptr };

    m_timer.Reset();

    while (msg.message != WM_QUIT)
    {
        // If there are Window messages then process them.
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        // Otherwise, do animation/game stuff.
        else
        {
            m_timer.Tick();

            if (!m_appPaused)
            {
                CalculateFrameStats();
                Update(m_timer);
                Draw(m_timer);
            }
            else
            {
                Sleep(100);
            }
        }
    }

    return static_cast<int>(msg.wParam);
}

float D3DApp::AspectRatio() const
{
    return static_cast<float>(m_clientWidth)/static_cast<float>(m_clientHeight);
}

D3DApp* D3DApp::GetApp()
{
    return g_theApp;
}

LRESULT D3DApp::MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    constexpr LRESULT success = 0;
    LRESULT output = success;
    switch (msg)
    {
        // WM_ACTIVATE is sent when the window is activated or deactivated.  
        // We pause the game when the window is deactivated and unpause it 
        // when it becomes active.  
    case WM_ACTIVATE:
    {
        if (LOWORD(wParam) == WA_INACTIVE)
        {
            m_appPaused = true;
            m_timer.Stop();
        }
        else
        {
            m_appPaused = false;
            m_timer.Start();
        }
        output = 0;
        break;
    }

        // WM_SIZE is sent when the user resizes the window.  
    case WM_SIZE:
    {
        // Save the new client area dimensions.
        m_clientWidth = LOWORD(lParam);
        m_clientHeight = HIWORD(lParam);
        if (m_device)
        {
            if (wParam == SIZE_MINIMIZED)
            {
                m_appPaused = true;
                m_minimized = true;
                m_maximized = false;
            }
            else if (wParam == SIZE_MAXIMIZED)
            {
                m_appPaused = false;
                m_minimized = false;
                m_maximized = true;
                OnResize();
            }
            else if (wParam == SIZE_RESTORED)
            {

                // Restoring from minimized state?
                if (m_minimized)
                {
                    m_appPaused = false;
                    m_minimized = false;
                    OnResize();
                }

                // Restoring from maximized state?
                else if (m_maximized)
                {
                    m_appPaused = false;
                    m_maximized = false;
                    OnResize();
                }
                else if (m_resizing)
                {
                    // If user is dragging the resize bars, we do not resize 
                    // the buffers here because as the user continuously 
                    // drags the resize bars, a stream of WM_SIZE messages are
                    // sent to the window, and it would be pointless (and slow)
                    // to resize for each WM_SIZE message received from dragging
                    // the resize bars.  So instead, we reset after the user is 
                    // done resizing the window and releases the resize bars, which 
                    // sends a WM_EXITSIZEMOVE message.
                }
                else // API call such as SetWindowPos or mSwapChain->SetFullscreenState.
                {
                    OnResize();
                }
            }
        }
        output = success;
        break;
    }

        // WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
    case WM_ENTERSIZEMOVE:
    {
        m_appPaused = true;
        m_resizing = true;
        m_timer.Stop();
        output = success;
        break;
    }

        // WM_EXITSIZEMOVE is sent when the user releases the resize bars.
        // Here we reset everything based on the new window dimensions.
    case WM_EXITSIZEMOVE:
    {
        m_appPaused = false;
        m_resizing = false;
        m_timer.Start();
        OnResize();
        output = success;
        break;
    }

        // WM_DESTROY is sent when the window is being destroyed.
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        output = success;
        break;
    }

        // The WM_MENUCHAR message is sent when a menu is active and the user presses 
        // a key that does not correspond to any mnemonic or accelerator key. 
    case WM_MENUCHAR:
    {
        // Don't beep when we alt-enter.
        output = MAKELRESULT(0, MNC_CLOSE);
        break;
    }

        // Catch this message so to prevent the window from becoming too small.
    case WM_GETMINMAXINFO:
    {
        reinterpret_cast<MINMAXINFO*>(lParam)->ptMinTrackSize.x = 200;  // NOLINT(performance-no-int-to-ptr)
        reinterpret_cast<MINMAXINFO*>(lParam)->ptMinTrackSize.y = 200;  // NOLINT(performance-no-int-to-ptr)
        output = success;
        break;
    }

    case WM_LBUTTONDOWN:
    case WM_MBUTTONDOWN:
    case WM_RBUTTONDOWN:
    {
        OnMouseDown(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        output = success;
        break;
    }
    case WM_LBUTTONUP:
    case WM_MBUTTONUP:
    case WM_RBUTTONUP:
    {
        OnMouseUp(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        output = success;
        break;
    }
    case WM_MOUSEMOVE:
    {
        OnMouseMove(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
        output = success;
        break;
    }
    case WM_KEYUP:
    {
        if (wParam == VK_ESCAPE)
        {
            PostQuitMessage(0);
        }
        else if (static_cast<int>(wParam) == VK_F2)
            Set4xMsaaState(!m_4xMsaaState);

        output = success;
        break;
    }
    default:
	{
        output = DefWindowProc(hwnd, msg, wParam, lParam);
        break;
	}
    }

    return output;
}

void D3DApp::InitializeDirect3D()
{
#if defined(DEBUG) || defined(_DEBUG)
    {
        ComPtr<ID3D12Debug> debugController;
        ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)));
        debugController->EnableDebugLayer();
    }
#endif

    ThrowIfFailed(CreateDXGIFactory1(IID_PPV_ARGS(&m_factory)));

    const auto hardwareResult = D3D12CreateDevice(
        nullptr,
        D3D_FEATURE_LEVEL_11_0,
        IID_PPV_ARGS(&m_device)
    );

    if (FAILED(hardwareResult))
    {
        ComPtr<IDXGIAdapter> pWarpAdapter;
        ThrowIfFailed(m_factory->EnumWarpAdapter(IID_PPV_ARGS(&pWarpAdapter)));
        ThrowIfFailed(D3D12CreateDevice(
            pWarpAdapter.Get(),
            D3D_FEATURE_LEVEL_11_0,
            IID_PPV_ARGS(&m_device))
        );
    }

    ThrowIfFailed(m_device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_fence)));

    m_rtvDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
    m_dsvDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
    m_cbvSrvDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

    D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msQualityLevels;
    msQualityLevels.Format = m_backBufferFormat;
    msQualityLevels.SampleCount = 4;
    msQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
    msQualityLevels.NumQualityLevels = 0;
    ThrowIfFailed(m_device->CheckFeatureSupport(
        D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS,
        &msQualityLevels,
        sizeof(msQualityLevels))
    );
    m_4xMsaaQuality = msQualityLevels.NumQualityLevels;
    assert(m_4xMsaaQuality > 0 && "Unexpected MSAA quality level.");

#ifdef _DEBUG
    LogAdapters();
#endif

    CreateCommandObjects();
    CreateSwapChain();
    CreateRtvAndDsvDescriptorHeaps();
}

void D3DApp::CreateCommandObjects()
{
    D3D12_COMMAND_QUEUE_DESC queueDesc = {};
    queueDesc.Type = COMMON_COMMAND_LIST_TYPE;
    queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    ThrowIfFailed(m_device->CreateCommandQueue(
        &queueDesc,
        IID_PPV_ARGS(&m_commandQueue))
    );

    ThrowIfFailed(m_device->CreateCommandAllocator(
        COMMON_COMMAND_LIST_TYPE, 
        IID_PPV_ARGS(m_directCmdListAlloc.GetAddressOf()))
    );

    ThrowIfFailed(m_device->CreateCommandList(
        0,
        COMMON_COMMAND_LIST_TYPE,
        m_directCmdListAlloc.Get(),
        nullptr,
        IID_PPV_ARGS(m_commandList.GetAddressOf()))
    );

    m_commandList->Close();
}

void D3DApp::CreateSwapChain()
{
    m_swapChain.Reset();

    DXGI_SWAP_CHAIN_DESC sd;
    sd.BufferDesc.Width = m_clientWidth;
    sd.BufferDesc.Height = m_clientHeight;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferDesc.Format = m_backBufferFormat;
    sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    sd.SampleDesc.Count = Get4xMsaaCount();
    sd.SampleDesc.Quality = Get4xMsaaQuality();
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.BufferCount = SWAP_CHAIN_BUFFER_COUNT;
    sd.OutputWindow = m_hMainWnd;
    sd.Windowed = true;
    sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

    ThrowIfFailed(m_factory->CreateSwapChain(m_commandQueue.Get(), &sd, m_swapChain.GetAddressOf()));
}

void D3DApp::FlushCommandQueue()
{
    ++m_currentFence;

    ThrowIfFailed(m_commandQueue->Signal(m_fence.Get(), m_currentFence));

    if (m_fence->GetCompletedValue() < m_currentFence)
    {
	    const HANDLE eventHandle = CreateEventEx(nullptr, nullptr, false, EVENT_ALL_ACCESS);  // NOLINT(misc-misplaced-const)
        if (eventHandle)
        {
            ThrowIfFailed(m_fence->SetEventOnCompletion(m_currentFence, eventHandle));
            WaitForSingleObject(eventHandle, INFINITE);
            CloseHandle(eventHandle);
        }
    }
}

ID3D12Resource* D3DApp::CurrentBackBuffer() const
{
    return m_swapChainBuffer[m_currentBackBuffer].Get();
}

void D3DApp::CreateRtvAndDsvDescriptorHeaps()
{
    D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
    rtvHeapDesc.NumDescriptors = SWAP_CHAIN_BUFFER_COUNT;
    rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
    rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    rtvHeapDesc.NodeMask = 0;
    ThrowIfFailed(m_device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(m_rtvHeap.GetAddressOf())));

    D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc;
    dsvHeapDesc.NumDescriptors = 1;
    dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
    dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    dsvHeapDesc.NodeMask = 0;
    ThrowIfFailed(m_device->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(m_dsvHeap.GetAddressOf())));
}

void D3DApp::OnResize()
{
    assert(m_device);
    assert(m_swapChain);
    assert(m_directCmdListAlloc);

    FlushCommandQueue();

    // Release the previous resources we will be recreating.
    ThrowIfFailed(m_commandList->Reset(m_directCmdListAlloc.Get(), nullptr));

    for (auto& buffer : m_swapChainBuffer)
    {
	    buffer.Reset();
    }
    m_depthStencilBuffer.Reset();

    // Resize the swap chain.
    ThrowIfFailed(m_swapChain->ResizeBuffers(
        SWAP_CHAIN_BUFFER_COUNT,
        m_clientWidth,  m_clientHeight,
        m_backBufferFormat,
        DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH));

    m_currentBackBuffer = 0;

    CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHeapHandle(m_rtvHeap->GetCPUDescriptorHandleForHeapStart());
    for (UINT i = 0; i < SWAP_CHAIN_BUFFER_COUNT; ++i)
    {
        ThrowIfFailed(m_swapChain->GetBuffer(i, IID_PPV_ARGS(&m_swapChainBuffer[i])));
        m_device->CreateRenderTargetView(m_swapChainBuffer[i].Get(), nullptr, rtvHeapHandle);
        rtvHeapHandle.Offset(1, m_rtvDescriptorSize);
    }

    D3D12_RESOURCE_DESC depthStencilDesc;
    depthStencilDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    depthStencilDesc.Alignment = 0;
    depthStencilDesc.Width = m_clientWidth;
    depthStencilDesc.Height = m_clientHeight;
    depthStencilDesc.DepthOrArraySize = 1;
    depthStencilDesc.MipLevels = 1;
    depthStencilDesc.Format = m_depthStencilFormat;
    depthStencilDesc.SampleDesc.Count = Get4xMsaaCount();
    depthStencilDesc.SampleDesc.Quality = Get4xMsaaQuality();
    depthStencilDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    depthStencilDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

    D3D12_CLEAR_VALUE optClear;
    optClear.Format = m_depthStencilFormat;
    optClear.DepthStencil.Depth = 1.0f;
    optClear.DepthStencil.Stencil = 0;
    CD3DX12_HEAP_PROPERTIES heapProperties(D3D12_HEAP_TYPE_DEFAULT);
    ThrowIfFailed(m_device->CreateCommittedResource(
        &heapProperties,
        D3D12_HEAP_FLAG_NONE,
        &depthStencilDesc,
        D3D12_RESOURCE_STATE_COMMON,
        &optClear,
        IID_PPV_ARGS(m_depthStencilBuffer.GetAddressOf())));

    m_device->CreateDepthStencilView(
        m_depthStencilBuffer.Get(),
        nullptr,
        DepthStencilView());

    CD3DX12_RESOURCE_BARRIER resourceBarrier = CD3DX12_RESOURCE_BARRIER::Transition(
        m_depthStencilBuffer.Get(),
        D3D12_RESOURCE_STATE_COMMON,
        D3D12_RESOURCE_STATE_DEPTH_WRITE);
    m_commandList->ResourceBarrier(1, &resourceBarrier);



    ThrowIfFailed(m_commandList->Close());
    ID3D12CommandList* commandsLists[] = { m_commandList.Get() };
    m_commandQueue->ExecuteCommandLists(_countof(commandsLists), commandsLists);
    
	FlushCommandQueue();

    m_screenViewport.TopLeftX = 0;
    m_screenViewport.TopLeftY = 0;
    m_screenViewport.Width = static_cast<float>(m_clientWidth);
    m_screenViewport.Height = static_cast<float>(m_clientHeight);
    m_screenViewport.MinDepth = 0.0f;
    m_screenViewport.MaxDepth = 1.0f;

    m_scissorRect = { 0, 0, m_clientWidth, m_clientHeight };
}

D3D12_CPU_DESCRIPTOR_HANDLE D3DApp::CurrentBackBufferView() const
{
    return CD3DX12_CPU_DESCRIPTOR_HANDLE(
        m_rtvHeap->GetCPUDescriptorHandleForHeapStart(),
        m_currentBackBuffer,
        m_rtvDescriptorSize);
}

D3D12_CPU_DESCRIPTOR_HANDLE D3DApp::DepthStencilView() const
{
    return m_dsvHeap->GetCPUDescriptorHandleForHeapStart();
}

UINT D3DApp::Get4xMsaaCount() const
{
    return m_4xMsaaState ? 4 : 1;
}

UINT D3DApp::Get4xMsaaQuality() const
{
    return m_4xMsaaState ? (m_4xMsaaQuality - 1) : 0;
}

void D3DApp::CalculateFrameStats() const
{
    // Code computes the average frames per second, and also the 
    // average time it takes to render one frame.  These stats 
    // are appended to the window caption bar.

    static int frameCount = 0;
    static float timeElapsed = 0.0f;

    ++frameCount;

    // Compute averages over one second period.
    if ((m_timer.GameTime() - timeElapsed) >= 1.0f)
    {
	    const auto fps = static_cast<float>(frameCount); // fps = frameCnt / 1
	    const float mspf = 1000.0f / fps;

	    const std::wstring fpsStr = std::to_wstring(fps);
	    const std::wstring mspfStr = std::to_wstring(mspf);

	    const std::wstring windowText = m_mainWndCaption +
            L"    fps: " + fpsStr +
            L"   mspf: " + mspfStr;

        SetWindowText(m_hMainWnd, windowText.c_str());

        // Reset for next average.
        frameCount = 0;
        timeElapsed += 1.0f;
    }
}

void D3DApp::LogAdapters() const
{
    std::vector<IDXGIAdapter*> adapterList;
    {
        UINT counter = 0;
        IDXGIAdapter* adapter = nullptr;
        while (m_factory->EnumAdapters(counter, &adapter) != DXGI_ERROR_NOT_FOUND)
        {
            DXGI_ADAPTER_DESC desc;
            adapter->GetDesc(&desc);

            std::wstring text = L"***Adapter: ";
            text += desc.Description;
            text += L"\n";

            OutputDebugString(text.c_str());

            adapterList.push_back(adapter);

            ++counter;
        }
    }

    for (auto& adapter : adapterList)
    {
        LogAdapterOutputs(adapter);
        ReleaseCom(adapter);
    }
}

void D3DApp::LogAdapterOutputs(IDXGIAdapter* adapter) const
{
    UINT counter = 0;
    IDXGIOutput* output = nullptr;
    while (adapter->EnumOutputs(counter, &output) != DXGI_ERROR_NOT_FOUND)
    {
        DXGI_OUTPUT_DESC desc;
        output->GetDesc(&desc);

        std::wstring text = L"***Output: ";
        text += desc.DeviceName;
        text += L"\n";
        OutputDebugString(text.c_str());

        LogOutputDisplayModes(output, m_backBufferFormat);

        ReleaseCom(output);

        ++counter;
    }
}

void D3DApp::LogOutputDisplayModes(IDXGIOutput* output, const DXGI_FORMAT format) const
{
    UINT count = 0;
    constexpr UINT flags = 0;

    // Call with nullptr to get list count.
    output->GetDisplayModeList(format, flags, &count, nullptr);

    std::vector<DXGI_MODE_DESC> modeList(count);
    output->GetDisplayModeList(format, flags, &count, &modeList[0]);

    for (const auto& mode : modeList)
    {
	    const UINT numerator = mode.RefreshRate.Numerator;
	    const UINT denominator = mode.RefreshRate.Denominator;
        std::wstring text =
            L"Width = " + std::to_wstring(mode.Width) + L" " +
            L"Height = " + std::to_wstring(mode.Height) + L" " +
            L"Refresh = " + std::to_wstring(numerator) + L"/" + std::to_wstring(denominator) +
            L"\n";

        ::OutputDebugString(text.c_str());
    }
}

bool D3DApp::Get4xMsaaState() const
{
    return m_4xMsaaState;
}

void D3DApp::Set4xMsaaState(const bool value)
{
    m_4xMsaaState = value;
}

bool D3DApp::InitializeMainWindow()
{
    WNDCLASS wc;
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = MainWndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = m_hAppInst;
    wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = static_cast<HBRUSH>(GetStockObject(NULL_BRUSH));
    wc.lpszMenuName = nullptr;
    wc.lpszClassName = L"MainWnd";

    if (!RegisterClass(&wc))
    {
        MessageBox(nullptr, L"RegisterClass Failed.", nullptr, DEFAULT_U_TYPE);
        return false;
    }

    // Compute window rectangle dimensions based on requested client area dimensions.
    RECT r = { 0, 0, static_cast<LONG>(m_clientWidth), static_cast<LONG>(m_clientHeight) };
    AdjustWindowRect(&r, WS_OVERLAPPEDWINDOW, false);
    const int width = r.right - r.left;
    const int height = r.bottom - r.top;

    m_hMainWnd = CreateWindow(
        L"MainWnd",
        m_mainWndCaption.c_str(),
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        width, height,
        nullptr, nullptr, 
        m_hAppInst, nullptr);

    if (!m_hMainWnd)
    {
        MessageBox(nullptr, L"CreateWindow Failed.", nullptr, DEFAULT_U_TYPE);
        return false;
    }

    ShowWindow(m_hMainWnd, SW_SHOW);
    UpdateWindow(m_hMainWnd);

    return true;
}
