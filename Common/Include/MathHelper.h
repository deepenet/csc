#pragma once
#include <DirectXMath.h>

namespace MathHelper
{
	DirectX::XMFLOAT4X4 Identity4x4();

	template<typename T>
	T Clamp(const T& x, const T& low, const T& high);
}



template<typename T>
T MathHelper::Clamp(const T& x, const T& low, const T& high)
{
	return x < low ? low : (x > high ? high : x);
}