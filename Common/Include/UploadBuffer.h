#pragma once
#include "d3dUtils.h"
#include "d3dx12.h"

template<typename T>
class UploadBuffer
{
public:
    UploadBuffer(ID3D12Device* device, UINT elementCount, bool isConstantBuffer);
    UploadBuffer(const UploadBuffer& other) = delete;
    UploadBuffer(UploadBuffer&& other) = delete;
    UploadBuffer& operator=(const UploadBuffer& rhs) = delete;
    UploadBuffer& operator=(UploadBuffer&& rhs) = delete;
    ~UploadBuffer();

    ID3D12Resource* Resource() const;

    void CopyData(int elementIndex, const T& data);

    UINT GetElementByteSize() const;

private:
    Microsoft::WRL::ComPtr<ID3D12Resource> m_uploadBuffer;
    BYTE* m_mappedData = nullptr;

    UINT m_elementByteSize = 0;
    bool m_isConstantBuffer = false;
};





template<typename T>
UploadBuffer<T>::UploadBuffer(ID3D12Device* device, UINT elementCount, bool isConstantBuffer) : m_isConstantBuffer(isConstantBuffer)
{
    m_elementByteSize = sizeof(T);
    // Constant buffer elements need to be multiples of 256 bytes.
    // This is because the hardware can only view constant data 
    // at m*256 byte offsets and of n*256 byte lengths. 
    // typedef struct D3D12_CONSTANT_BUFFER_VIEW_DESC {
    // UINT64 OffsetInBytes; // multiple of 256
    // UINT   SizeInBytes;   // multiple of 256
    // } D3D12_CONSTANT_BUFFER_VIEW_DESC;
    if (isConstantBuffer)
    {
        m_elementByteSize = d3dUtil::CalcConstantBufferByteSize(sizeof(T));
    }

    ThrowIfFailed(device->CreateCommittedResource(
        &CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
        D3D12_HEAP_FLAG_NONE,
        &CD3DX12_RESOURCE_DESC::Buffer((UINT64)elementCount * (UINT64)m_elementByteSize),
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(m_uploadBuffer.GetAddressOf())));

    ThrowIfFailed(m_uploadBuffer->Map(0, nullptr, reinterpret_cast<void**>(&m_mappedData)));
    // We do not need to unmap until we are done with the resource.  However, we must not write to
    // the resource while it is in use by the GPU (so we must use synchronization techniques).
}

template<typename T>
UploadBuffer<T>::~UploadBuffer()
{
    if (m_uploadBuffer)
    {
        m_uploadBuffer->Unmap(0, nullptr);
    }
    m_uploadBuffer = nullptr;
}

template<typename T>
inline ID3D12Resource* UploadBuffer<T>::Resource() const
{
    return m_uploadBuffer.Get();
}

template<typename T>
inline void UploadBuffer<T>::CopyData(int elementIndex, const T& data)
{
    ::memcpy(&m_mappedData[m_elementByteSize * elementIndex], &data, sizeof(T));
}

template<typename T>
inline UINT UploadBuffer<T>::GetElementByteSize() const
{
    return m_elementByteSize;
}
