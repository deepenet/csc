#pragma once
#include "d3dUtils.h"
#include "MathHelper.h"
#include "UploadBuffer.h"
#include <memory>

struct ObjectConstants
{
    DirectX::XMFLOAT4X4 World = MathHelper::Identity4x4();
    DirectX::XMFLOAT4X4 Bones[2];
};

struct PassConstants
{
    DirectX::XMFLOAT4X4 ViewProj = MathHelper::Identity4x4();
    float TotalTime = 0.0f;
};

struct Vertex
{
    DirectX::XMFLOAT4 Color;
    DirectX::XMFLOAT3 Pos;
    float Weight;
    DirectX::XMFLOAT3 SecondAnimationPos;
    DirectX::XMFLOAT3 Normal;
};


// Stores the resources needed for the CPU to build the command lists
// for a frame.  
struct FrameResource
{
    FrameResource() = delete;
    FrameResource(ID3D12Device* device, UINT passCount, UINT objectCount, UINT materialCount);
    FrameResource(const FrameResource& rhs) = delete;
    FrameResource(FrameResource&& rhs) = delete;
    FrameResource& operator=(const FrameResource& rhs) = delete;
    FrameResource& operator=(FrameResource&& rhs) = delete;
    ~FrameResource() = default;

    // We cannot reset the allocator until the GPU is done processing the commands.
    // So each frame needs their own allocator.
    Microsoft::WRL::ComPtr<ID3D12CommandAllocator> CmdListAlloc;

    // We cannot update a cbuffer until the GPU is done processing the commands
    // that reference it.  So each frame needs their own cbuffers.
    std::unique_ptr<UploadBuffer<PassConstants>> PassCB = nullptr;
    std::unique_ptr<UploadBuffer<ObjectConstants>> ObjectCB = nullptr;
    std::unique_ptr<UploadBuffer<MaterialConstants>> MaterialCB = nullptr;

    // Fence value to mark commands up to this fence point.  This lets us
    // check if these frame resources are still in use by the GPU.
    UINT64 Fence = 0;
};

