#pragma once
#include <windows.h>
#include <wrl.h>
#include <string>
#include <d3d12.h>
#include <unordered_map>
#include <DirectXMath.h>

class DxException
{
public:
    DxException() = default;
    DxException(HRESULT hr, std::wstring functionName, std::wstring filename, int lineNumber);

    std::wstring ToString()const;

    HRESULT ErrorCode = S_OK;
    std::wstring FunctionName;
    std::wstring Filename;
    int LineNumber = -1;
};

inline std::wstring AnsiToWString(const std::string& str)
{
    WCHAR buffer[512];
    MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, buffer, 512);
    return std::basic_string<wchar_t>(buffer);
}

namespace d3dUtil
{
    Microsoft::WRL::ComPtr<ID3D12Resource> CreateDefaultAndInitUploadBuffers(
        ID3D12Device* device,
        ID3D12GraphicsCommandList* cmdList,
        const void* initData,
        UINT64 byteSize,
        Microsoft::WRL::ComPtr<ID3D12Resource>& uploadBuffer);

    UINT CalcConstantBufferByteSize(UINT size);

    Microsoft::WRL::ComPtr<ID3DBlob> CompileShader(
        const std::wstring& filename,
        const D3D_SHADER_MACRO* defines,
        const std::string& entryPoint,
        const std::string& target);

    Microsoft::WRL::ComPtr<ID3DBlob> LoadBinary(const std::wstring& filename);

    Microsoft::WRL::ComPtr<ID3DBlob> TryLoadOrCompileShader(
        const std::wstring& compiledFilename,
        const std::wstring& sourceFilename,
        const D3D_SHADER_MACRO* defines,
        const std::string& entryPoint,
        const std::string& target);
}

struct SubmeshGeometry
{
    UINT IndexCount = 0;
    UINT StartIndexLocation = 0;
    INT BaseVertexLocation = 0;
};

struct MeshGeometry
{
    std::string Name;

    // System memory copies.  Use Blobs because the vertex/index format can be generic.
    // It is up to the client to cast appropriately. 
    Microsoft::WRL::ComPtr<ID3DBlob> VertexBufferCpu = nullptr;
    Microsoft::WRL::ComPtr<ID3DBlob> IndexBufferCpu = nullptr;

    Microsoft::WRL::ComPtr<ID3D12Resource> VertexBufferGpu = nullptr;
    Microsoft::WRL::ComPtr<ID3D12Resource> IndexBufferGpu = nullptr;

    Microsoft::WRL::ComPtr<ID3D12Resource> VertexBufferUploader = nullptr;
    Microsoft::WRL::ComPtr<ID3D12Resource> IndexBufferUploader = nullptr;

    UINT VertexByteStride = 0;
    UINT VertexBufferByteSize = 0;
    DXGI_FORMAT IndexFormat = DXGI_FORMAT_R16_UINT;
    UINT IndexBufferByteSize = 0;

    // A MeshGeometry may store multiple geometries in one vertex/index buffer.
    // Use this container to define the Submesh geometries so we can draw
    // the Submeshes individually.
    std::unordered_map<std::string, SubmeshGeometry> DrawArgs;

    D3D12_VERTEX_BUFFER_VIEW VertexBufferView() const;
    D3D12_INDEX_BUFFER_VIEW IndexBufferView() const;

    // We can free this memory after we finish upload to the GPU.
    void DisposeUploaders();
};

struct Light
{
    DirectX::XMFLOAT3 Strength = { 0.5f, 0.5f, 0.5f };
    float FalloffStart = 1.0f;
    DirectX::XMFLOAT3 Direction = { 0.0f, -1.0f, 0.0f };
    float FalloffEnd = 10.0f;
    DirectX::XMFLOAT3 Position = { 0.0f, 0.0f, 0.0f };
    float SpotPower = 64.0f;
};

constexpr int MAX_LIGHTS = 16;

struct MaterialConstants
{
    DirectX::XMFLOAT4 DiffuseAlbedo = { 1.0f, 1.0f, 1.0f, 1.0f };
    DirectX::XMFLOAT3 FresnelR0 = { 0.01f, 0.01f, 0.01f };
    float Roughness = 0.25f;
};

struct Material
{
    std::string Name;

    int MatCBIndex = -1;
    int DiffuseSrvHeapIndex = -1;
    int NormalSrvHeapIndex = -1;
    int NumFramesDirty = 1;

    MaterialConstants Constants;
};

#ifndef ThrowIfFailed
#define ThrowIfFailed(x)                                              \
{                                                                     \
    HRESULT hr__ = (x);                                               \
    std::wstring wfn = AnsiToWString(__FILE__);                       \
    if(FAILED(hr__)) { throw DxException(hr__, L#x, wfn, __LINE__); } \
}
#endif

#ifndef ThrowIfFalse
#define ThrowIfFalse(x)                                                 \
{                                                                       \
    std::wstring wfn = AnsiToWString(__FILE__);                         \
    if(false == (x)) { throw DxException(E_FAIL, L#x, wfn, __LINE__); } \
}
#endif

#ifndef ThrowIfNullptr
#define ThrowIfNullptr(x)                                                   \
{                                                                           \
    std::wstring wfn = AnsiToWString(__FILE__);                             \
    if(nullptr == (x)) { throw DxException(E_FAIL, L#x, wfn, __LINE__); }   \
}
#endif

#ifndef ReleaseCom
#define ReleaseCom(x) { if(x){ x->Release(); x = 0; } }
#endif
