#pragma once
#include <d3d12.h>
#include <dxgi1_4.h>
#include "DXHelper.h"
#include "GameTimer.h"

#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib, "D3D12.lib")
#pragma comment(lib, "dxgi.lib")

class D3DApp
{
public:
	D3DApp() = delete;
	D3DApp(HINSTANCE hInstance);
	D3DApp(const D3DApp& rhs) = delete;
	D3DApp(D3DApp&& rhs) = delete;
	D3DApp& operator=(const D3DApp& rhs) = delete;
	D3DApp& operator=(D3DApp&& rhs) = delete;
	virtual ~D3DApp() = default;

	static D3DApp* GetApp();

	int Run();

	float AspectRatio() const;

	bool Get4xMsaaState() const;
	void Set4xMsaaState(bool value);

	virtual bool Initialize();
	virtual LRESULT MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

protected:

	virtual void CreateRtvAndDsvDescriptorHeaps();
	virtual void OnResize();
	virtual void Update(const GameTimer& gt) = 0;
	virtual void Draw(const GameTimer& gt) = 0;

	virtual void OnMouseDown(WPARAM btnState, int x, int y) { }
	virtual void OnMouseUp(WPARAM btnState, int x, int y)   { }
	virtual void OnMouseMove(WPARAM btnState, int x, int y) { }

protected:

	bool InitializeMainWindow();
	void InitializeDirect3D();
	void CreateCommandObjects();
	void CreateSwapChain();

	void FlushCommandQueue();

	ID3D12Resource* CurrentBackBuffer() const;
	D3D12_CPU_DESCRIPTOR_HANDLE CurrentBackBufferView() const;
	D3D12_CPU_DESCRIPTOR_HANDLE DepthStencilView() const;

	UINT Get4xMsaaCount() const;
	UINT Get4xMsaaQuality() const;

	void CalculateFrameStats() const;

	void LogAdapters() const;
	void LogAdapterOutputs(IDXGIAdapter* adapter) const;
	void LogOutputDisplayModes(IDXGIOutput* output, DXGI_FORMAT format) const;

protected:
	HINSTANCE m_hAppInst = nullptr;
	HWND      m_hMainWnd = nullptr;
	bool      m_appPaused = false;
	bool      m_minimized = false;
	bool      m_maximized = false;
	bool      m_resizing = false;
	bool      m_fullscreenState = false;

	std::wstring m_mainWndCaption = L"d3d App";

protected:
	static constexpr UINT SWAP_CHAIN_BUFFER_COUNT = 2;

	ComPtr<IDXGIFactory4>				m_factory;
	ComPtr<IDXGISwapChain>				m_swapChain;
	ComPtr<ID3D12Device>				m_device;
	ComPtr<ID3D12CommandQueue>			m_commandQueue;
	ComPtr<ID3D12CommandAllocator>		m_directCmdListAlloc;
	ComPtr<ID3D12GraphicsCommandList>	m_commandList;
	ComPtr<ID3D12DescriptorHeap>		m_rtvHeap;
	ComPtr<ID3D12DescriptorHeap>		m_dsvHeap;
	ComPtr<ID3D12Resource>				m_swapChainBuffer[SWAP_CHAIN_BUFFER_COUNT];
	ComPtr<ID3D12Resource>				m_depthStencilBuffer;

	ComPtr<ID3D12Fence>					m_fence;
	UINT64 m_currentFence = 0;

	D3D12_VIEWPORT m_screenViewport{};
	D3D12_RECT m_scissorRect{};

	UINT m_rtvDescriptorSize = 0;
	UINT m_dsvDescriptorSize = 0;
	UINT m_cbvSrvDescriptorSize = 0;

	UINT m_currentBackBuffer = 0;

	bool m_4xMsaaState = false;
	UINT m_4xMsaaQuality = 0;

	GameTimer m_timer;

	int m_clientWidth = 1280;
	int m_clientHeight = 720;

	DXGI_FORMAT m_backBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
	DXGI_FORMAT m_depthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

	static constexpr D3D12_COMMAND_LIST_TYPE COMMON_COMMAND_LIST_TYPE = D3D12_COMMAND_LIST_TYPE_DIRECT;
};

